import logo from "./logo.svg";
import "./App.css";
import ExShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
function App() {
  return (
    <div className="App">
      <ExShoeShop />
    </div>
  );
}

export default App;
