import React, { Component } from "react";

class ItemShoe extends Component {
  shoeData = this.props.shoeData;
  /**
  shoeArr: {
    id: number;
    name: string;
    alias: string;
    price: number;
    description: string;
    shortDescription: string;
    quantity: number;
    image: string;
} 
 */
  render() {
    let { image, name, alias, price, description, shortDescription } =
      this.props.shoeData;
    return (
      <div className="col-3">
        <div className="card" style={{ width: "100%" }}>
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              {description.length < 30
                ? description
                : description.slice(0, 30) + "..."}
            </p>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.shoeData);
              }}
              className="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ItemShoe;
