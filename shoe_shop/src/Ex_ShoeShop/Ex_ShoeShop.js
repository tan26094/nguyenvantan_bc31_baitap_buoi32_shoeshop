import React, { Component } from "react";
import { shoeArr } from "./data_ShoeShop";
import ItemShoe from "./ItemShoe";
import TableGioHang from "./TableGioHang";
class ExShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };
  renderShoes = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ItemShoe
          handleAddToCart={this.handleAddToCart}
          shoeData={item}
          key={item.id}
        />
      );
    });
  };
  handleAddToCart = (sp) => {
    // console.log("Yes");
    console.log(sp);
    console.log("gioHang: ", this.state.gioHang);
    // let cloneGioHang = [...this.state.gioHang, sp];
    // cloneGioHang.push(sp);
    // this.setState({ gioHang: cloneGioHang });
    // Trường hợp 1: sp chưa có trong giỏ hàng ==> tạo object mới, có thêm trường soLuong
    // let newSp = { ...sp, soLuong: 1 };
    // Trường hợp 2: SP đã có trong giỏ hàng ==> dự vào index, soLuong
    // gioHang[index].soLuong++
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    console.log("index: ", index);
    if (index == -1) {
      let newSp = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  handleRemoveShoe = (idShoe) => {
    console.log(idShoe);
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handleAdjustQuantity = (idShoe, adjust) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (cloneGioHang[index].soLuong == 1) {
      cloneGioHang.splice(index, 1);
    } else {
      cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + adjust;
    }
    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    return (
      <div className="container py-5">
        {this.state.gioHang.length > 0 && (
          <TableGioHang
            handleRemoveShoe={this.handleRemoveShoe}
            handleAdjustQuantity={this.handleAdjustQuantity}
            gioHang={this.state.gioHang}
          ></TableGioHang>
        )}
        <div className="row">{this.renderShoes()}</div>
      </div>
    );
  }
}

export default ExShoeShop;
